"""
Worker node program

The worker will periodically query the main API for jobs that it can work. Once a job is retrieved,
it will execute the job's code and return the ouput to the API.

Configurable via:
  - API_HOST = hostname and port of main API server (default: 'localhost:5000')
  - BACKOFF_SECONDS = number of seconds to wait after no jobs are returned from API (default: 15)
"""

import os
import time
import datetime
import sys
from io import StringIO
import contextlib
import json
import traceback

import requests

API_HOST = os.environ.get('API_HOST', 'localhost:5000') # Default to dev server
API_BASE = 'http://{}/api/v1'.format(API_HOST)
BACKOFF_SECONDS = os.environ.get('BACKOFF_SECONDS', 15)

def print_ts(msg):
    """Helper function for printing strings with a leading timestamp"""

    print('[{}] {}'.format(datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), msg))

def get_job():
    """Check the API for a job we can execute"""

    # Build the url
    endpoint = '{}/job/pending'.format(API_BASE)
    # Execute the request
    try:
        response = requests.get(endpoint)
    except requests.exceptions.RequestException as req_err:
        print_ts('Unable to get pending jobs!')
        print(req_err)

    # Handle response
    if response.status_code == requests.codes['ok']:
        print_ts('Found job:')
        print(response.text)
        return response.json()

    return None

@contextlib.contextmanager
def redirect_stdout(stdout=None):
    """Temporarily change the system stdout to a stringIO pipe so we can grab output to return"""
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

def execute_job(job_info):
    """Run the specified job, monitor status, record output"""

    success = True
    start_time = datetime.datetime.now()
    with redirect_stdout() as string_stdout:
        try:
            exec(job_info['code']) # pylint: disable=exec-used
        except: # pylint: disable=bare-except
            print('Encountered an exception while executing!')
            traceback.print_exc(file=string_stdout)
            success = False
    end_time = datetime.datetime.now()

    job_results = {
        'output': string_stdout.getvalue(),
        'success': success,
        'start_time': start_time,
        'end_time': end_time
    }

    return job_results

def report_job_status(job_info, job_results):
    """Report back to the API and let it know how the job went"""

    params = {
        'id': job_info['id'],
        'status': 'complete' if job_results['success'] else 'error'
    }

    payload = json.dumps(
        {
            'output': job_results['output'],
            'start_time': job_results['start_time'],
            'end_time': job_results['end_time']
        },
        default=str # Convert any unserializable objects (datetimes in our case) to strings
    )

    # Build the url
    endpoint = '{}/job/complete'.format(API_BASE)
    # Execute the request
    try:
        response = requests.put(
            endpoint,
            params=params,
            data=payload,
            headers={'Content-type': 'application/json'}
        )
    except requests.exceptions.RequestException as req_err:
        print_ts('Error when submitting job results!')
        print(req_err)

    if response.status_code != 200:
        print_ts('ERROR: Recieved non 200 code when submitting job status!')
        print(response.text)
    else:
        print_ts('Job status successfully reported')

def main():
    """Script main function, contains worker logic loop"""
    while True:
        print_ts('Checking for jobs...')
        job = get_job()

        if job is not None:
            # We got a job, execute it
            print_ts('Executing recieved job...')
            results = execute_job(job)

            # Report the result
            print_ts('Reporting job results...')
            report_job_status(job, results)
        else:
            # No job was waiting, sleep before checking again
            print_ts('No job found, sleeping for {} seconds...'.format(BACKOFF_SECONDS))
            time.sleep(BACKOFF_SECONDS)

if __name__ == "__main__":
    main()
