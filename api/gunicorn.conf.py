"""
Gunicorn settings
"""

# Stop pylint from complaining about setting names
# pylint: disable=invalid-name
bind = "0.0.0.0:8000"
accesslog = '-' # Print access info to stdout
