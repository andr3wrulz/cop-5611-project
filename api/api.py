"""
Flask API for queueing and retrieving job status

Listens on port 5000 (flask default)
Flask runs the debug server (hot reloads) if the FLASK_ENV environment var is set to "development"
"""

import datetime
import sqlite3

from flask import Flask, request, jsonify
from apscheduler.schedulers.background import BackgroundScheduler

from flask_cors import CORS

DATABASE = 'jobs.db'

APP = Flask(__name__)
CORS(APP)

def init_db():
    """Function called at app start to ensure jobs table exists"""

    con = sqlite3.connect(DATABASE)
    # Set up jobs table if the table doesn't exist (new database)
    con.execute(
        'CREATE TABLE IF NOT EXISTS jobs ('
        'id INTEGER PRIMARY KEY,'
        'submit_time DATETIME,'
        'code TEXT NOT NULL,'
        'start_time DATETIME NULL,'
        'end_time DATETIME NULL,'
        'output TEXT NULL,'
        'status TEXT,'
        'dispatched DATETIME NULL,'
        'timeout INT,'
        'tries INT,'
        'max_tries INT)'
    )
    con.close()

def init_schedule():
    """Function called at app start to register failed job scheduled task"""

    print('Registering failed job scheduled function')
    sched = BackgroundScheduler(daemon=True)
    # Run at every 15 second interval
    sched.add_job(check_for_failed_jobs, 'cron', second='*/15')
    sched.start()

def check_for_failed_jobs():
    """Scheduled function to marked jobs that are over their alloted retries and timed out as failed"""

    print('[{}] Checking for failed jobs...'.format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

    try:
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        query = '''UPDATE jobs
            SET status = 'timed out'
            WHERE
                status = 'pending' AND
                tries >= max_tries AND
                CAST(strftime('%s', '{time_now}') AS integer) > CAST(strftime('%s', dispatched) AS integer) + timeout
            '''.format(time_now=datetime.datetime.now())
        cur.execute(query)
        updated = cur.rowcount
        con.commit()

        con.close()
    except sqlite3.Error as sql_error:
        print('ERROR: Sqlite error during check_for_failed_jobs: {}'.format(sql_error))
        return

    print('[{}] Marked {} jobs as failed'.format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), updated))

def convert_row_to_dict(row):
    """Conversion function from array returned from sql to dict based on job table schema"""

    job = {
        'id': row[0],
        'submit_time': row[1],
        'code': row[2],
        'start_time': row[3],
        'end_time': row[4],
        'output': row[5],
        'status': row[6],
        'dispatched': row[7],
        'timeout': row[8],
        'tries': row[9],
        'max_tries': row[10]
    }
    return job

@APP.route('/api/v1/job/all', methods=['GET'])
def get_all_jobs():
    """API endpoint for returning all jobs from the database"""

    try:
        con = sqlite3.connect(DATABASE)
        results = con.execute('SELECT * FROM jobs ORDER BY submit_time DESC;').fetchall()
        con.close()
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    jobs = []
    for job in results:
        jobs.append(convert_row_to_dict(job))

    return jsonify(jobs), 200

@APP.route('/api/v1/job', methods=['GET'])
def get_job_by_id():
    """API endpoint for returning a specific job from the database"""

    # Check if id was passed
    if 'id' in request.args:
        job_id = int(request.args['id'])
    else:
        return 'ERROR: Please specify an id!'

    try:
        con = sqlite3.connect(DATABASE)
        job = con.execute('SELECT * FROM jobs WHERE id =? LIMIT 1', (job_id,)).fetchone()
        con.close()
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    if job is None:
        return 'ERROR: No job found with id {}'.format(id), 204 # No content

    return jsonify(convert_row_to_dict(job)), 200

@APP.route('/api/v1/job', methods=['POST'])
def submit_job():
    """API endpoint for submitting a job to be executed"""

    try:
        req = request.json
        job_timeout = req.get('timeout', 180)
        max_tries = req.get('retries', 2) + 1
        submit_time = datetime.datetime.now()
        params = (req['code'], submit_time, 'pending', job_timeout, max_tries)

        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row

        cur = con.cursor()
        query = '''INSERT INTO jobs
            (code, submit_time, status, timeout, max_tries, tries)
            VALUES(?, ?, ?, ?, ?, 0)'''
        cur.execute(query, params)

        inserted = {
            'id': cur.lastrowid,
            'submit_time': submit_time,
            'status': 'pending',
            'timeout': job_timeout,
            'max_tries': max_tries
        }
        print('Inserted job id: {}'.format(cur.lastrowid))

        con.commit()
        con.close()
    except KeyError:
        return 'ERROR: Invalid input data!', 400 # Bad request
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    return inserted, 201 # Created

@APP.route('/api/v1/job/pending', methods=['GET'])
def get_pending_job():
    """API endpoint for workers to get the next job they should work.
        Returns the oldest non-dispatched job or the olded dispatched job"""

    try:
        con = sqlite3.connect(DATABASE)
        # Note: NULL in sqlite is treated as smaller than any value
        query = '''SELECT * FROM jobs
            WHERE
                status="pending" AND
                (
                    dispatched IS NULL OR
                    CAST(strftime('%s', '{time_now}') AS integer) > CAST(strftime('%s', dispatched) AS integer) + timeout
                ) AND
                tries < max_tries
            ORDER BY dispatched, submit_time
            LIMIT 1'''.format(time_now=datetime.datetime.now())
        job = con.execute(query).fetchone()

        # Check if job was found
        if job is None:
            con.close()
            return 'ERROR: No queued jobs', 204 # No content

        # If there was a job, update the dispatch time
        query = '''UPDATE jobs
            SET dispatched = ?,
                tries = tries + 1
            WHERE id = ?'''
        params = (datetime.datetime.now(), job[0])
        con.execute(query, params)
        con.commit()

        con.close()
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    # Return job info to worker
    return jsonify(convert_row_to_dict(job)), 200

@APP.route('/api/v1/job/complete', methods=['PUT'])
def complete_job():
    """API endpoint for workers to report a completed job"""

    # Parse request data to ensure we have what we need
    try:
        job_id = int(request.args['id'])
        job_output = request.json['output']
        job_start = request.json['start_time']
        job_end = request.json['end_time']
    except KeyError:
        return 'ERROR: Invalid syntax! Required fields: id, output, start_time, end_time', 400 # Bad request

    # Default job status to complete
    job_status = request.args.get('status', 'complete')

    try:
        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row

        cur = con.cursor()
        params = (job_status, job_start, job_end, job_output, job_id)
        query = '''UPDATE jobs
            SET status = ?,
                start_time = ?,
                end_time = ?,
                output = ?
            WHERE
                id = ? AND
                status = 'pending'
            '''
        cur.execute(query, params)

        if cur.rowcount == 0:
            return 'ERROR: Invalid job id or job is already finished!', 400 # Bad request

        con.commit()
        con.close()
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    return 'Marked job {} as complete!'.format(job_id), 200 # OK

@APP.route('/api/v1/job/cancel', methods=['PUT'])
def cancel_job():
    """API endpoint for cancelling a job"""

    # Check if id was passed
    if 'id' in request.args:
        job_id = int(request.args['id'])
    else:
        return 'ERROR: Please specify an id!'

    try:
        con = sqlite3.connect(DATABASE)
        con.row_factory = sqlite3.Row

        cur = con.cursor()
        params = (datetime.datetime.now(), job_id)
        query = '''UPDATE jobs
            SET status = 'cancelled',
                end_time = ?
            WHERE
                id = ? AND
                status = 'pending'
            '''
        cur.execute(query, params)

        if cur.rowcount == 0:
            return 'ERROR: Invalid job id or job is already finished!', 400 # Bad request

        con.commit()
        con.close()
    except sqlite3.Error as sql_error:
        return 'ERROR: Sqlite error: {}'.format(sql_error), 500 # Internal server error

    return 'Cancelled job {}'.format(job_id), 200 # OK

# Create the job table if it doesn't exist
init_db()

# Register the background scheduler for failed jobs
init_schedule()

# If this file was ran directly from the command line, run the dev server
if __name__ == "__main__":
    # Have to disable the dev auto-reloader or the scheduled function will register multiple times :/
    APP.run(debug=True, use_reloader=False)
