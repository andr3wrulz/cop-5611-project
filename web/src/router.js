import VueRouter from 'vue-router'

import Home from './components/Home'
import Job from './components/Job'
import Submit from './components/Submit'

const routes = [
  { path: '/', component: Home },
  { path: '/submit', component: Submit },
  { path: '/job/:id', component: Job }
]

const router = new VueRouter({
  routes
})

// Workaround for debouncing duplicated push errors
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => {
    if (err.name !== 'NavigationDuplicated') throw err
  });
}

export default router