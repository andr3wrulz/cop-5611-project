import Vue from 'vue'
import VueRouter from 'vue-router'
import VueHighlightJS from 'vue-highlight.js'

import router from './router' // imports router object after building routes
import App from './App.vue'

// Highlight.js languages (All languages)
import 'vue-highlight.js/lib/allLanguages'
// Import Highlight.js theme
import 'highlight.js/styles/default.css'

Vue.use(VueRouter)
Vue.use(require('vue-moment'))
Vue.use(VueHighlightJS)
Vue.config.productionTip = false

Vue.prototype.$apiHostname = 'https://cop5611api.andrewgeltz.com'

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
