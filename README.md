# COP 5611 Spring 2021 Project - Serverless Python
This semester project was an attempt to implement a "serverless" system to execute python code (similar to AWS Lambda). Serverless systems remove the managing of underlying resources from the end user allowing them to focus on their code.

## Architecture
The system has three components, each runs inside a container (there can be multiple workers run simultaneously) built using a GitLab CI pipeline [.gitlab-ci.yml](.gitlab-ci.yml):
* API (Flask with SQLite database)
* Workers (python script)
* UI (Vue.js)

![Architecture](images/architecture.png)


## Screenshots
### Job list
![Job List](images/job_list.png)
### Submitting code
![Submitting code](images/sample_submit.png)
### Code executing
![Code executing](images/executing.png)
### Code finished executing
![Code finished executing](images/executed.png)

## References
- [SQLite Tutorial for Python](https://www.sqlitetutorial.net/sqlite-python/)
- [Requests module quickstart guide](https://docs.python-requests.org/en/master/user/quickstart/)